from itertools import chain, combinations, permutations
from typing import List

def powerset_from_iterable(iterable) -> List[List]:
    s = list(iterable)
    powerset = chain.from_iterable(combinations(s, r) for r in range(len(s)+1))
    return list(map(lambda x: list(x), list(powerset)))

def permutations_from_iterable(iterable) -> List[List]:
    permutations_list = list(permutations(iterable))
    permutations_list = list(map(lambda x: list(x), permutations_list))
    return permutations_list