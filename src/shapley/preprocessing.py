import numpy as np
import pandas as pd
from typing import List
from .sklearn_types import PreprocessingPipeline, CategoricalEncoder

class Preprocessor:
    def __init__(self, preprocessing_pipeline: PreprocessingPipeline, categorical_encoder: CategoricalEncoder = None, categorical_variables: List[str] = None):

        parameters_are_invalid = (categorical_encoder and not categorical_variables) or (not categorical_encoder and categorical_variables)
        if( parameters_are_invalid ): raise Exception('Specify both or none of categorical variables/encoder')

        self.preprocessing_pipeline = preprocessing_pipeline
        self.categorical_variables= categorical_variables
        self.categorical_encoder = categorical_encoder

    def encode_categorical(self, X_train: pd.DataFrame, X_test: pd.DataFrame, variables_to_encode: List[str]) -> tuple[np.ndarray, np.ndarray]:
            if(not self.categorical_encoder): raise Exception('You must specify a categorical encoder')
            train_rows = len(X_train)
            X_train_cat = X_train[variables_to_encode]
            X_test_cat = X_test[variables_to_encode]
            X_cat_complete: np.ndarray = np.concatenate((X_train_cat, X_test_cat), axis=0)
            X_cat_enc = self.categorical_encoder.fit_transform(X_cat_complete).toarray()
            X_train_cat_enc: np.ndarray = X_cat_enc[:train_rows]
            X_test_cat_enc: np.ndarray = X_cat_enc[train_rows:]
            return X_train_cat_enc, X_test_cat_enc

    def preprocess_with_categorical(self, X_train: pd.DataFrame, X_test: pd.DataFrame, variables: List[str]) -> tuple[np.ndarray, np.ndarray]:
            variables_to_encode = list(filter(lambda var: var in self.categorical_variables, variables))
            variables_other = list(filter(lambda var: var not in self.categorical_variables, variables))
            X_train_cat_enc, X_test_cat_enc = self.encode_categorical(X_train, X_test, variables_to_encode)

            if(not len(variables_other)):
                return X_train_cat_enc, X_test_cat_enc

            X_train_std = self.preprocessing_pipeline.fit_transform(X_train[variables_other])
            X_test_std = self.preprocessing_pipeline.transform(X_test[variables_other])
            X_train_processed: np.ndarray = np.concatenate((X_train_std, X_train_cat_enc), axis=1)
            X_test_processed: np.ndarray = np.concatenate((X_test_std, X_test_cat_enc), axis=1)
            return X_train_processed, X_test_processed
    
    def preprocess_standard(self, X_train: pd.DataFrame, X_test: pd.DataFrame,  variables: List[str]) -> tuple[np.ndarray, np.ndarray]:
            X_train_processed = self.preprocessing_pipeline.fit_transform(X_train[variables])
            X_test_processed = self.preprocessing_pipeline.transform(X_test[variables])
            return X_train_processed, X_test_processed

    #Preprocessing method, behaviour changes accordingly to the presence of categoriacal variables
    def preprocess(self, X_train: pd.DataFrame, X_test: pd.DataFrame,  variables: List[str]) -> tuple[np.ndarray, np.ndarray]:

        are_there_any_categorical = self.categorical_encoder and any(variable in self.categorical_variables for variable in variables)

        if(are_there_any_categorical):
            X_train_processed, X_test_processed = self.preprocess_with_categorical(X_train, X_test,  variables)
        else:
            X_train_processed, X_test_processed = self.preprocess_standard(X_train, X_test,  variables)

        return  X_train_processed, X_test_processed