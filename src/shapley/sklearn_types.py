from typing import Protocol
import numpy as np

class Model(Protocol):
    def fit(self, X, y, **kwargs) -> None:
        """Fit the model to the data"""

    def predict(self, X, **kwargs) -> np.ndarray:
        """Produce model output for input data"""


class PreprocessingPipeline(Protocol):
    def fit(self, X, **kwargs) -> None:
        """Fit the preprocessing pipeline to the data"""

    def fit_transform(self, X, **kwargs) -> np.ndarray:
        """Fit the preprocessing pipeline to the data produce preprocessing pipeline output for input data"""

    def transform(self, X, **kwargs) -> np.ndarray:
        """Produce preprocessing pipeline output for input data"""


class EncodedDataset(Protocol):
    def toarray(self, **kwargs) -> np.ndarray:
        """return encoded categorical variables to array"""

class CategoricalEncoder(Protocol):
    def fit(self, X, **kwargs) -> None:
        """Fit the preprocessing pipeline to the data"""

    def fit_transform(self, X, **kwargs) -> EncodedDataset:
        """Fit the preprocessing pipeline to the data produce preprocessing pipeline output for input data"""

    def transform(self, X, **kwargs) -> np.ndarray:
        """Produce preprocessing pipeline output for input data"""

