import string
from tokenize import String
import pandas as pd
import os
from typing import List, Tuple, TypedDict, Union

def make_data_loader(realtive_datasetpath: str, target_variable: str, categorical_variables: List[str] = None, colum_to_drop_idxs: List[int] = None):

    class LoadedData(TypedDict):
        data: Tuple[pd.DataFrame, pd.Series]
        categorical_variables: Union[List[str], None]
    
    def load_dataset():
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, realtive_datasetpath)
        dataset = pd.read_csv(filename)

        y = dataset[target_variable]
        labels_to_drop = [target_variable] if not colum_to_drop_idxs else [dataset.columns[idx] for idx in colum_to_drop_idxs] + [target_variable]
        X = dataset.drop(labels_to_drop, axis=1)

        loaded_data: LoadedData = { 'data': [X, y], 'categorical_variables': categorical_variables}
        return loaded_data

    return load_dataset


load_Credit_dataset = make_data_loader('datasets/Credit.csv', 'Balance', colum_to_drop_idxs=[0], categorical_variables=['Gender', 'Student', 'Married', 'Ethnicity'])

load_Income2_dataset =  make_data_loader('datasets/Income2.csv', 'Income', colum_to_drop_idxs=[0])

load_iris_regression_dataset = make_data_loader('datasets/iris.csv', 'Petal.Width', colum_to_drop_idxs=[0], categorical_variables=['Species'])

load_WomanWage = make_data_loader('datasets/WomenWage.csv', 'hours') 