import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import f_regression
import matplotlib.pyplot as plt
from .misc import permutations_from_iterable, powerset_from_iterable
from .preprocessing import Preprocessor
from .sklearn_types import Model
from typing import Callable, List,TypedDict

class ShapleyResult(TypedDict):
    variable_name: str
    value: float

class ShapleyCalculator:
    def __init__(self, X: pd.DataFrame, y: pd.DataFrame, preprocessor: Preprocessor, model: Model, score_fn: Callable[[np.ndarray, np.ndarray], float], is_score_error: bool):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
        self.X_train: np.ndarray = X_train
        self.X_test: np.ndarray  = X_test
        self.y_train: np.ndarray  = y_train
        self.y_test: np.ndarray  = y_test
        self.variables: List[str] = X.columns
        self.preprocessor = preprocessor
        self.model = model
        self.score_fn = score_fn
        self.results: List[ShapleyResult] = None
        self.is_score_error = is_score_error
    
    
    #Given a list of variables this method returns the score of the model made of those variables
    def get_score_for_variables(self, variables: List[str]) -> float:
        X_train_processed, X_test_processed = self.preprocessor.preprocess(self.X_train, self.X_test, variables)    
        self.model.fit(X_train_processed, self.y_train)
        y_pred = self.model.predict(X_test_processed)
        score = self.score_fn(self.y_test, y_pred)
        return score
    

    #Non-optimized procedure for computing shapley values
    def get_shapley_values_standard(self) -> List[ShapleyResult]:

        permutations = permutations_from_iterable(self.variables)

        def get_shapley_value_for_variable(variable: str) -> float:
            to_avg = map(lambda permutation: get_marginal_contrib_for_permutation(permutation, variable), permutations)
            mean: float = np.mean(list(to_avg))
            return mean

        def get_marginal_contrib_for_permutation(permutation: List[str], variable: str) -> float:
            curr_var_idx = permutation.index(variable)
            preceding_vars = permutation[:curr_var_idx]
            
            score_preceding_vars = self.get_score_for_variables(preceding_vars) if(len(preceding_vars)) else 0.
            score_curr_var = self.get_score_for_variables(preceding_vars+[variable])

            marginal_contrib = score_curr_var - score_preceding_vars
            return marginal_contrib if(not self.is_score_error) else - marginal_contrib

        shapley_values: List[ShapleyResult] = list(map(lambda var: {'variable_name': var, 'value': get_shapley_value_for_variable(var)}, self.variables))
        self.results = shapley_values
        return self.results


    #Optimized procedure for computing shapley values
    def get_shapley_values_optimized(self) -> List[ShapleyResult]:
        
        def get_shapley_value_for_variable(variable: str):
            other_variables = list(filter(lambda var: var != variable, self.variables))
            powerset = powerset_from_iterable(other_variables)
            n_variables = len(self.variables)
            to_avg = list(map(lambda set: get_marginal_contrib_for_set_in_powerset(set, variable, n_variables), powerset))
            mean: float = np.sum(list(to_avg))/math.factorial(n_variables)
            return mean

        
        def get_marginal_contrib_for_set_in_powerset(set: List[str], variable: str, total_variables: int) -> float:
            other_variables = list(filter(lambda var: var != variable, set))
            K = total_variables
            T = len(other_variables)
            
            score_preceding_vars = self.get_score_for_variables(other_variables) if(len(other_variables)) else 0.
            score_curr_var = self.get_score_for_variables(other_variables+[variable])

            coeff = math.factorial(T)*math.factorial(K - T - 1)
            marginal_contrib = coeff * (score_curr_var - score_preceding_vars)
            return marginal_contrib

        shapley_values = map(lambda var: {'variable_name': var, 'value': get_shapley_value_for_variable(var)}, self.variables)
        self.results = list(shapley_values)
        return self.results



    #Choose the procudere to use when computing shapley values Optimized/non-optimized
    def get_shapley_values(self, optimized: bool = False) -> List[ShapleyResult]:

        if optimized:
            result = self.get_shapley_values_optimized()
        else:
            result = self.get_shapley_values_standard()

        return result

    
    #get the score of the complete model(considering all the variable)
    def get_full_score(self) -> float:
        return self.get_score_for_variables(self.variables)
    
    
    #Compute p-values for the regression model
    def get_p_values(self) -> np.ndarray:
        #how to compute p-values for categorical variables?
        X_train_processed, _ = self.preprocessor.preprocess(self.X_train, self.X_test, self.variables)
        _, p_values = f_regression(X_train_processed, self.y_train) 

        return p_values


    #Print a summary containing all the usefull info of an experiment
    def print_summary(self):
        if( not self.results ): raise Exception('Cannot print summary before computing results!')

        print("Shapley calculator summary:\n")
        print(f"Scoring function: {self.score_fn}\n")
        print(f"Score of the complete model: {self.get_full_score()}\n")
        print(f"Sum of the decomposed scores: {np.sum(list(map(lambda var: var['value'], self.results)))}\n")
        print("Shapley values for variable  (Score decomposition):\nVariable name\tShapley-value(score)\n")
        [print("{: >10}\t{: >10}".format(var['variable_name'], var['value'])) for var in self.results]
        print()
        print("p-values:\nVariable name\tp-value\n")
        [print("{: >10}\t{: >10}".format(variable_name, p_value)) for variable_name, p_value in zip(self.variables, sorted(self.get_p_values()))]


    #Plot the shapley values
    def plot_shapley(self):
        if( not self.results ): raise Exception('Cannot print summary before computing results!')
        self.results.sort(reverse=True, key=(lambda x: x['value']))
        full_value = self.get_full_score()
        labels = list(map(lambda x: x['variable_name'], self.results))
        y_pos = np.arange(len(labels))
        values = list(map(lambda x: x['value']/full_value * 100, self.results))
        fig, ax = plt.subplots()
        ax.barh(y_pos, values, align='center')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(labels)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Percentage of total score')
        ax.set_title('Shapley values')
